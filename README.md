Project using the base of [Jasper client project](https://github.com/jasperproject/jasper-client)

Those plugins are supposed to be added to the `plugins/speechhandler` folder of the Jasper installation

The installation can be done using the Ansible playbook [ansible-jasper](https://gitlab.com/boardcrew/ansible-jasper)
