# -*- coding: utf-8 -*-
import collections
import requests
import json
from jasper import app_utils
from jasper import plugin

INSTALADOR_URL = 'https://instalador.devops-spms.xyz/installation'
#INSTALADOR_LIST_URL = self.profile['instalador']['listurl']

AppComponent = collections.namedtuple('AppComponent', ['name'])

def get_app_components():
    r = requests.get(INSTALADOR_URL)
    response = r.json()
    response_components = response['applicationComponents']
    components = []
    for i, response_component in enumerate(response_components, start=1):
        components.append(AppComponent(name=response_component['name']))   
    return components

class InstaladorListPlugin(plugin.SpeechHandlerPlugin):
    
    def __init__(self, *args, **kwargs):
        super(InstaladorListPlugin, self).__init__(*args, **kwargs)

    def get_priority(self):
        return 4

    def get_phrases(self):
        return [
            self.gettext("LIST"),
            self.gettext("COMPONENTS")]

    def handle(self, text, mic):
        mic.say(self.gettext("Getting the installation components..."))

        appcomps = get_app_components()
        if len(appcomps) == 0:
            mic.say(self.gettext(
                "Sorry, I'm unable to get the application components from installer"))
            return

        text = self.gettext('These are the application components ...')
        text += ' '
        text += '... '.join(
            '%s' % (appcomp.name)
            for i, appcomp in enumerate(appcomps, start=1))
        mic.say(text)

    def is_valid(self, text):
        return (self.gettext('LIST COMPONENTS').upper() in text.upper())
