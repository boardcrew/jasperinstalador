# -*- coding: utf-8 -*-
import collections
import requests
import json
from jasper import app_utils
from jasper import plugin

INSTALADOR_LIST_URL = 'https://instalador.devops-spms.xyz/installation'
INSTALADOR_URL = 'https://instalador.devops-spms.xyz/component/%s'

#INSTALADOR_LIST_URL = self.profile['instalador']['listurl']
#INSTALADOR_URL = self.profile['instalador']['installurl']

AppComponent = collections.namedtuple('AppComponent', ['name'])

def get_app_components():
    r = requests.get(INSTALADOR_LIST_URL)
    response = r.json()
    response_components = response['applicationComponents']
    components = []
    for i, response_component in enumerate(response_components, start=1):
        components.append(AppComponent(name=response_component['name'].upper()))   
    return components

def get_answers_as_components(answers):
    anscomps = []
    for i,answer in enumerate(answers, start=1):
        anscomps.append(AppComponent(name=answer.upper())) 
    return anscomps

class InstaladorInstallPlugin(plugin.SpeechHandlerPlugin):
    
    def __init__(self, *args, **kwargs):
        super(InstaladorInstallPlugin, self).__init__(*args, **kwargs)

    def get_priority(self):
        return 4

    def get_phrases(self):
        return [self.gettext("INSTALL")]

    def handle(self, text, mic):
        mic.say(self.gettext("Which component do you want to install?"))
        anscomps = get_answers_as_components(mic.active_listen())
        print("anscomps: %s" % anscomps)
        mic.say(self.gettext("Looking up the available components."))
        appcomps = get_app_components()  
        print("appcomps: %s" % appcomps)
        matchcomps = list(set(appcomps) & set(anscomps))
        print("matchcomps: %s" % matchcomps)
        
        if (len(matchcomps) > 1):
            text = self.gettext('Too many components matches your selection, they are ...')
            text += ' '
            text += '... '.join(
                '%s' % (matchcomp.name)
                for i, matchcomp in enumerate(matchcomps, start=1))
            mic.say(text)
            return

        if (len(matchcomps) < 1):
            mic.say(self.gettext('Nothing matches your selection'))
            return

        if (len(matchcomps) == 1):
            component = matchcomps[0]
            mic.say(self.gettext("%s found, confirm %s installation?" % (component.name, component.name)))
            confirmations = mic.active_listen()
            if any(self.gettext('YES').upper() in confirmation.upper() for confirmation in confirmations):                    
                mic.say(self.gettext("Installing %s" % component.name))
                print(INSTALADOR_URL % component.name.lower())
                r = requests.post(INSTALADOR_URL % component.name.lower())
                print("response: %s" % r.content)
                response = r.json()
                response_phase = response['phase']
                mic.say(self.gettext(response_phase))
            else:
                mic.say(self.gettext("OK, not installing %s" % component.name))
        else:
            mic.say(self.gettext("Something weird happened!"))

    def is_valid(self, text):
        return (self.gettext('INSTALL').upper() in text.upper())
